﻿#include <iostream>
#include <string>
#include <future>
#include <chrono>
#include <boost/program_options.hpp>

#include "../Signature/Signature.h"
#include "../Signature/Reactor.h"

static constexpr uint64_t DEFAULT_BLOCK_SIZE = 1024ull * 1024;

// Helper class to calculate elapsed time for specified operation.
class StopWatch
{
    std::chrono::steady_clock::time_point m_start;

public:
    StopWatch() noexcept
    {
        m_start = std::chrono::steady_clock::now();
    }

    uint64_t ElapsedMs() const noexcept
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - m_start).count();
    }
};

int main(int argc, char** argv)
{
    namespace po = boost::program_options;
    // Arguments will be stored here
    std::string input;
    std::string output;
    uint64_t blockSize = 0;

    // Configure options here
    po::options_description desc("Signature tool. \nUsage: SignatureTool -i <input file> -o <output file> [-b  <block size>]\nOptions");
    desc.add_options()
        ("help,h", "Print help")
        ("input,i", po::value(&input), "Input file")
        ("output,o", po::value(&output), "Output file")
        ("block_size,b", po::value(&blockSize), "Block size");

    // Parse command line arguments
    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
        po::notify(vm);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Invalid parameters provided, reason: " <<  e.what() << "\n\n"
                  << desc << std::endl;

        return 1;
    }

    // Check if there are enough args or if --help is given
    if (vm.count("help") || !vm.count("input") || !vm.count("output"))
    {
        std::cerr << desc << std::endl;
        return 1;
    }

    if (0 == blockSize)
        blockSize = DEFAULT_BLOCK_SIZE;

    StopWatch sw;

    // Create console logger
    auto logger = CreateLogger(std::cout);
    auto fileSigner = CreateFileSigner(logger, input.c_str(), output.c_str(), blockSize);

    auto promise = std::make_shared<std::promise<bool>>();
    auto future = promise->get_future();

    auto lambda = [promise](bool ok)
        {
            promise->set_value(ok);
        };
    
    const bool result = fileSigner->AsyncMakeSignature(lambda);

    if (!result)
        promise->set_value(false);

    if (!future.get())
        std::cerr << "Failed make signature. Elapsed " << sw.ElapsedMs() << "ms" << std::endl;
    else
        std::cout << "Finished make signature. Elapsed " << sw.ElapsedMs() << "ms" << std::endl;


    // TODO: use error codes
    return result ? 0 : 1;
    
}
