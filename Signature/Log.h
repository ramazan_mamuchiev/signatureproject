#pragma once
#include <memory>
#include <sstream>

// Interface for log interface.
class ILogger
{
public:
    virtual ~ILogger() {}
    
    virtual void Log(const char* log) = 0;
};

#define LOG_BASE(logger, formatString) \
    do \
    { \
        ILogger* const mylogger = logger; \
        if (!mylogger) \
            break; \
        std::ostringstream s; \
        s << formatString; \
        mylogger->Log( s.str().c_str()); \
    } while(false) 

#define LOG(formatString) LOG_BASE(getLogger(), formatString)

std::shared_ptr<ILogger> CreateLogger(std::ostream& ostream);