﻿#include "Signature.h"

#include <queue>
#include <map>
#include <mutex>

#include <boost/uuid/detail/md5.hpp>
#include <boost/algorithm/hex.hpp>

#include "Log.h"
#include "ReaderWriter.h"
#include "Reactor.h"

namespace
{
const unsigned int READ_THREAD_COUNT = 1;

class FinishContext
{
    finishedHandler_t m_handler;
    bool m_ok;
public:

    FinishContext(finishedHandler_t handler) :
        m_handler(handler),
        m_ok(false)
    {}

    void setResult(bool ok) { m_ok = ok; }

    ~FinishContext()
    {
        if (m_handler)
            m_handler(m_ok);
    }
};

class Block
{
    std::shared_ptr<ILogger> m_logger;
    std::shared_ptr<uint8_t> m_data;
    uint64_t m_usedSize = 0;
    uint64_t m_blockId = 0;

private:

    std::shared_ptr<uint8_t> allocateMemory(uint64_t blockSizeBytes)
    {
        std::shared_ptr<uint8_t> block;

        try
        {
            block = std::shared_ptr<uint8_t>(new uint8_t[blockSizeBytes], std::default_delete<uint8_t[]>());
        }
        catch (const std::bad_alloc& e)
        {
            LOG("Could not allocate requested memory for requested block size = " << blockSizeBytes << ", error: " << e.what());
            return std::shared_ptr<uint8_t>();
        }

        return block;
    }

    ILogger* getLogger()
    {
        return m_logger.get();
    }

public:

    Block(std::shared_ptr<ILogger> logger) :
        m_logger(logger)
    {
    }

    bool Allocate(uint64_t blockSizeBytes)
    {
       m_data = allocateMemory(blockSizeBytes);
       return (m_data != nullptr);
    }

    void SetUsedSize(uint64_t usedSize)
    {
        m_usedSize = usedSize;
    }

    uint64_t GetUsedSize() const
    {
        return m_usedSize;
    }

    uint8_t* Data()
    {
       return m_data.operator->();
    }
    void SetBlockId(uint64_t blockId)
    {
        m_blockId = blockId;
    }
    uint64_t GetBlockId() const
    {
        return m_blockId;
    }
};


typedef std::shared_ptr<FinishContext> FinishContextSP;

class FileSigner : public IFileSigner
{
    std::shared_ptr<ILogger> m_logger;
    std::shared_ptr<IReactor> m_readReactor;
    std::shared_ptr<IReactor> m_hashReactor;
    const std::string m_inFileName;
    const std::string m_outFileName;
    const uint64_t m_blockSizeBytes;

    std::shared_ptr<IFileReader> m_reader;
    std::shared_ptr<IFileWriter> m_writer;

    std::mutex m_mutex;
    std::map<uint64_t, std::string> m_hashQueue;

    bool m_success;
    uint64_t m_blocksCount;
    uint64_t m_totalBlocksCount;

    std::mutex m_finishCtxMutex;
    FinishContextSP m_finishCtx;

    bool m_needWaitHashPerforming;
    uint64_t m_currentBlockId = 0;
    uint64_t m_expectedHashId = 0;

    uint32_t m_readedBlockCount = 0;

public:
    FileSigner(std::shared_ptr<ILogger> logger, const char* inFileName, const char* outFileName, uint64_t blockSizeBytes) :
        m_logger(logger),
        m_readReactor(CreateReactor(READ_THREAD_COUNT)),
        m_hashReactor(CreateReactor()),
        m_inFileName(inFileName),
        m_outFileName(outFileName),
        m_blockSizeBytes(blockSizeBytes),
        m_success(false),
        m_blocksCount(0),
        m_totalBlocksCount(0),
        m_needWaitHashPerforming(false)
    {}

    bool AsyncMakeSignature(finishedHandler_t handler) override
    {
        try
        {
            m_reader = CreateFileReader(m_inFileName, m_blockSizeBytes);
            m_writer = CreateFileWriter(m_outFileName);
        }
        catch (const std::exception& e)
        {
            LOG("Error working with file, reason: " << e.what());
            return false;
        }
        auto fileSize = m_reader->GetSize();
        m_totalBlocksCount = fileSize / m_blockSizeBytes;

        if (fileSize % m_blockSizeBytes)
            ++m_totalBlocksCount;

        m_finishCtx = std::make_shared<FinishContext>(handler);
        m_readReactor->Ioc().post(std::bind(&FileSigner::asyncReadBlock, this, m_finishCtx));

        return true;
    }

private:
    void asyncReadBlock(FinishContextSP finishCtx)
    {

        {
            std::lock_guard<std::mutex> lock(m_mutex);
            if (!m_needWaitHashPerforming && m_readedBlockCount >= m_hashReactor->ThreadCount())
            {
                m_needWaitHashPerforming = true;
                return;
            }
            ++m_readedBlockCount;
        }

        Block block(m_logger);
        if (!block.Allocate(m_blockSizeBytes))
        {
            finalize();
            return;
        }
        
        uint64_t readSize = 0;
        bool readSuccess = false;

        try
        {
             readSuccess = m_reader->ReadNextBlock(block.Data(), readSize);
             
            // this will lead to invoke success finished handler after last write
            if (!readSuccess)
            {
                m_success = true;
                finalize();
                return;
            }
        }
        catch (const std::exception& e)
        {
            m_success = false;
            LOG("Error working with blocks, reason: " << e.what());
        }

        if (!readSuccess)
        {
            finalize();
            return;
        }

        block.SetUsedSize(readSize);
        block.SetBlockId(m_currentBlockId++);

        m_hashReactor->Ioc().post(std::bind(&FileSigner::asyncGenerateHash, this, finishCtx, block));
        m_readReactor->Ioc().post(std::bind(&FileSigner::asyncReadBlock, this, finishCtx));
    }

    void asyncGenerateHash(FinishContextSP finishCtx, Block block)
    {

        auto hashString = getHash(block.Data(), block.GetUsedSize());
        if (hashString.empty())
        {
            LOG("ERROR: Skipped blockId: " << block.GetBlockId() << ", reason: hash string empty");
            return;
        }

        std::lock_guard<std::mutex> lock(m_mutex);
        
        --m_readedBlockCount;

        // continue reading after previous stop reading
        if (m_needWaitHashPerforming && m_readedBlockCount <= m_hashReactor->ThreadCount() / 2)
        {
            m_needWaitHashPerforming = false;
            m_readReactor->Ioc().post(std::bind(&FileSigner::asyncReadBlock, this, finishCtx));
        }

        m_hashQueue.insert(std::make_pair(block.GetBlockId(), hashString));

        // reduce io_service post operations count if we cannot perform write operation
        if (block.GetBlockId() != m_expectedHashId)
            return;  

        m_hashReactor->Ioc().post(std::bind(&FileSigner::asyncWriteHash, this, finishCtx));
    }

    void asyncWriteHash(FinishContextSP finishCtx)
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        if (m_hashQueue.empty())
            return;

        std::string hashStrings;
        do
        {
            auto nextHashIt = m_hashQueue.begin();
            if (nextHashIt->first != m_expectedHashId)
                break;

            ++m_expectedHashId;
            hashStrings += nextHashIt->second;
            m_hashQueue.erase(nextHashIt);
        } while (!m_hashQueue.empty());

        if (hashStrings.empty())
        {
            // empty m_finishCtx means that read operation is competed, but not performed yet all hash operations
             if (!m_finishCtx)
                 m_hashReactor->Ioc().post(std::bind(&FileSigner::asyncWriteHash, this, finishCtx));

            return;
        }

        try
        {
            m_writer->Append(reinterpret_cast<const uint8_t*>(hashStrings.c_str()), hashStrings.size());
        }
        catch (const std::exception& e)
        {
            LOG("Error writing hash, reason: " << e.what());
            return;
        }
 
        LOG("Performed block " << ++m_blocksCount << " of " << m_totalBlocksCount);
    }

    void finalize()
    {
        std::lock_guard<std::mutex> lock(m_finishCtxMutex);

        if (!m_finishCtx)
            return;

        m_finishCtx->setResult(m_success);
        m_hashReactor->Ioc().post(std::bind(&FileSigner::asyncWriteHash, this, m_finishCtx));
        m_finishCtx.reset();
    }

    ILogger* getLogger()
    {
        return m_logger.get();
    }

    std::string getHash(uint8_t* data, uint64_t size)
    {
        using boost::uuids::detail::md5;
        md5 hash;
        md5::digest_type digest;

        hash.process_bytes(data, size);
        hash.get_digest(digest);

        const auto charDigest = reinterpret_cast<const char *>(&digest);
        std::string result;
        boost::algorithm::hex(charDigest, charDigest + sizeof(md5::digest_type), std::back_inserter(result));
        return result;
    }
};

}

std::shared_ptr<IFileSigner> CreateFileSigner(std::shared_ptr<ILogger> logger, const char* inFileName, const char* outFileName, uint64_t blockSizeBytes)
{
    return std::make_shared<FileSigner>(logger, inFileName, outFileName, blockSizeBytes);
}

