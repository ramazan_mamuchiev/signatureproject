#include "ReaderWriter.h"

#include <fstream>

#include "Log.h"

namespace 
{
// RAII file reader
class FileReader : public IFileReader
{
private:
    std::ifstream m_ifs;
    std::string m_filename;

    uint64_t m_size;
    uint64_t m_offset;
    const uint64_t m_blockSize;

    uint64_t m_totalElapsedMs;

public:
    FileReader(const std::string& filename, uint64_t blockSize) :
        m_filename(filename),
        m_size(0),
        m_offset(0),
        m_blockSize(blockSize),
        m_totalElapsedMs(0)
    {
        m_ifs.open(filename, std::ios_base::in | std::ios_base::binary);

        if (!m_ifs.is_open())
            throw std::runtime_error(std::string("cannot open input file: ") + m_filename);

        m_ifs.exceptions(std::ios_base::failbit | std::ios_base::badbit);

        m_ifs.seekg(0, std::ios_base::end);
        m_size = m_ifs.tellg();
        m_ifs.seekg(0, std::ios_base::beg);
    }

    bool ReadNextBlock(uint8_t* outBuffer, uint64_t& readSize) override
    {
        if (m_offset >= m_size)
            return false;

        uint64_t calculatedSize = m_blockSize;
        if (m_blockSize > m_size)
            calculatedSize = m_size;
        else if (m_blockSize > (m_size - m_offset))
            calculatedSize = m_size - m_offset;

        try
        {
            m_ifs.read(reinterpret_cast<char*>(outBuffer), calculatedSize);
            readSize = calculatedSize;
            m_offset += calculatedSize;
        }
        catch (const std::exception& e)
        {
            throw std::runtime_error(std::string("error reading: ") + m_filename + ", reason: " + e.what());
        }
        return true;
    }

    uint64_t GetSize() const override
    {
        return m_size;
    }

    ~FileReader()
    {
        if (m_ifs.is_open())
            m_ifs.close();
    }
};

class FileWriter : public IFileWriter
{
    std::ofstream m_ofs;
    std::string m_filename;

public:

    FileWriter(const std::string& filename) :
        m_filename(filename)
    {
        m_ofs.open(filename, std::ios_base::trunc | std::ios_base::binary | std::ios_base::out);

        if (!m_ofs.is_open())
            throw std::runtime_error(std::string("cannot open output file: ") + m_filename);

        m_ofs.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    }

    void Append(const uint8_t* addr, uint64_t size) override
    {
        try
        {
            m_ofs.write(reinterpret_cast<const char*>(addr), size);
        }
        catch (std::exception const& e)
        {
            throw std::runtime_error("error writing to " + m_filename + ", reason: "+ e.what());
        }
    }

    ~FileWriter()
    {
        if (m_ofs.is_open())
            m_ofs.close();
    }
};

}

std::shared_ptr<IFileReader> CreateFileReader(const std::string& filename, uint64_t blockSize)
{
    return std::make_shared<FileReader>(filename, blockSize);
}

std::shared_ptr<IFileWriter> CreateFileWriter(const std::string& filename)
{
    return std::make_shared<FileWriter>(filename);
}