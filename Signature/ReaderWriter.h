#pragma once
#include <memory>
#include <string>

// Interface for file reader.
class IFileReader
{
public:
    virtual ~IFileReader() {}

    // Read one block to initialized outBuffer.
    virtual bool ReadNextBlock(uint8_t* outBuffer, uint64_t& readSize) = 0;

    // Get total file size.
    virtual uint64_t GetSize() const = 0;
};

// Interface for file writer.
class IFileWriter
{
public:
    virtual ~IFileWriter() {}

    // Append buffer at end of file.
    virtual void Append(const uint8_t* addr, uint64_t size) = 0;
};

std::shared_ptr<IFileReader> CreateFileReader(const std::string& filename, uint64_t blockSize);

std::shared_ptr<IFileWriter> CreateFileWriter(const std::string& filename);

