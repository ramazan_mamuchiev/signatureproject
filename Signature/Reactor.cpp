#include "Reactor.h"

#include<boost/thread.hpp>
#include<boost/bind.hpp>

class Reactor : public IReactor
{
public:
    Reactor(unsigned int threadCount) :
        m_threadCount(threadCount)
    {
        m_work = std::make_unique<boost::asio::io_context::work>(m_ioc);
        for (size_t i = 0; i < m_threadCount; ++i)
            m_threadPool.create_thread(boost::bind(&boost::asio::io_context::run, &m_ioc));
    }
    ~Reactor()
    {
        m_work.reset();

        try
        {
            m_threadPool.join_all();
        }
        catch (const std::exception&)
        {

        }
    }

    boost::asio::io_context& Ioc() override
    {
        return m_ioc;
    }

    unsigned int ThreadCount() const override
    {
        return m_threadCount;
    }

private:
    boost::asio::io_context m_ioc;
    std::unique_ptr<boost::asio::io_context::work> m_work;
    boost::thread_group m_threadPool;
    unsigned int m_threadCount;
};


std::shared_ptr<IReactor> CreateReactor(unsigned int threadCount)
{
    if (0 == threadCount)
        threadCount = boost::thread::hardware_concurrency();

    return std::make_shared<Reactor>(threadCount);

}
