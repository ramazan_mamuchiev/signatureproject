﻿#pragma once

#include <functional>

#include<boost/asio/io_context.hpp>

#include "Log.h"

typedef std::function<void(bool)> finishedHandler_t;

// Interface for file signer.
class IFileSigner
{
public:
    virtual ~IFileSigner() {}

    // Schedule make signature.
    // Returns true if schedule success, otherwire return false.
    // Invoke passed handler when finished perform operation.
    virtual bool AsyncMakeSignature(finishedHandler_t handler) = 0;
};

std::shared_ptr<IFileSigner> CreateFileSigner(std::shared_ptr<ILogger> logger, const char* inFileName, const char* outFileName, uint64_t blockSizeBytes);
