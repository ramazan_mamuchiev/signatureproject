#include "Log.h"

#include <thread>
#include <chrono>
#include <time.h>

namespace
{
std::string GetlocalTime()
{
    time_t now = time(0);
    tm tstruct;
    char buf[80];
    localtime_s(&tstruct, &now);

    strftime(buf, sizeof(buf), "%D %T", &tstruct);

    return buf;
}

class Logger : public ILogger
{
    std::ostream& m_ostream;

public:
    Logger(std::ostream& ostream) :
        m_ostream(ostream)
    {}

    void Log(const char* log) override
    {
        m_ostream << "[" << std::this_thread::get_id() << "][" << GetlocalTime() << "] " << log << std::endl;
    }

};
}

std::shared_ptr<ILogger> CreateLogger(std::ostream& ostream)
{
    return std::make_shared<Logger>(ostream);
}