#pragma once
#include<memory>

#include<boost/asio/io_context.hpp>

// Interface for tasks shceduler class based on boost::asio::io_context
class IReactor
{
public:
    virtual ~IReactor() {}

    virtual boost::asio::io_context& Ioc() = 0;
    virtual unsigned int ThreadCount() const = 0;
};

// 0 - means boost::thread::hardware_concurrency()
std::shared_ptr<IReactor> CreateReactor(unsigned int threadCount = 0);